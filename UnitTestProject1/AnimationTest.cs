﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AniLib.Animations;
using AniLib.Math;

namespace UnitTestProject1
{
    [TestClass]
    public class AnimationTest
    {
        [TestMethod]
        public void TestElementAdding()
        {
            Animation controller = new Animation();
            controller.AddNewElement();
            if (!controller.Elements.Any())
            {
                Assert.Fail("Was not added");
            }
        }

        [TestMethod]
        public void TestElementRemoving()
        {
            Animation controller = new Animation();

            Element element = controller.AddNewElement();

           
            controller.Remove(element);

            if (controller.Elements.Any())
            {
                Assert.Fail("Was not removed");
            }
        }

        [TestMethod]
        public void TestAnimationPlaying()
        {
            Animation animation = new Animation();

            Element element = animation.AddNewElement();

            element.SetFrame(ElementFrameHelper.CreateWithPosition(new Vector2(0, 0), 0.0f));
            element.SetFrame(ElementFrameHelper.CreateWithPosition(new Vector2(1, 0), 1.0f));

            Element element_2 = animation.AddNewElement();

            element_2.SetFrame(ElementFrameHelper.CreateWithPosition(new Vector2(1, 0), 0.0f));
            element_2.SetFrame(ElementFrameHelper.CreateWithPosition(new Vector2(1, 1), 1.0f));


            for (float time = 0; time <= 1.0f; time+=0.1f)
            {
                animation.InterpolateElementsTo(time);

                Assert.AreEqual(element.CurrentStatus.Position.X, time);
                Assert.AreEqual(element.CurrentStatus.Position.Y, 0.0f);
                Assert.AreEqual(element_2.CurrentStatus.Position.X, 1.0f);
                Assert.AreEqual(element_2.CurrentStatus.Position.Y, time);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AniLib.Animations;
using AniLib.Math;

namespace UnitTestProject1
{
    public static class ElementFrameHelper
    {
        public static ElementFrame CreateWithPosition(Vector2 position, float time)
        {
            return new ElementFrame
            {
                Alpha = 1.0f,
                Position = position,
                Rotation = new Vector2(0, 0),
                Time = time
            };
        }

        public static ElementFrame CreateFrameElementWithRotation(Vector2 rotation, float time)
        {
            return new ElementFrame
            {
                Alpha = 1.0f,
                Position = new Vector2(),
                Rotation = rotation,
                Time = time
            };
        }

        public static void CheckPositionInterpolation(ElementFrame interpolatedFrame, Vector2 expected)
        {
            Vector2 position = interpolatedFrame.Position;
            CheckInterpolation(position, expected);
        }

        public static void CheckRotationInterpolation(ElementFrame interpolatedFrame, float angleDeg)
        {
            Vector2 rotation = interpolatedFrame.Rotation;

            float angleRad = angleDeg * Mathf.DegToRad;

            Vector2 expected = new Vector2(Mathf.Cos(angleRad), -Mathf.Sin(angleRad));
            CheckInterpolation(rotation, expected);
        }

        public static void CheckInterpolation(Vector2 real, Vector2 expected)
        {
            Assert.IsTrue(Math.Abs(real.X - expected.X) < 0.01f);
            Assert.IsTrue(Math.Abs(real.Y - expected.Y) < 0.01f);
        }


    }
}

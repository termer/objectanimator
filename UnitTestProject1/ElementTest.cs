﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AniLib.Animations;
using AniLib.Math;

namespace UnitTestProject1
{
    [TestClass]
    public class ElementTest
    {
        [TestMethod]
        public void TestFrameAdding()
        {
            const float cTime = 1.0f;

            Element element = new Element(0);

            ElementFrame frame = ElementFrameHelper.CreateWithPosition(new Vector2(1, 1), cTime);

            element.SetFrame(frame);

            ElementFrame frameFromElement = element.GetFrame(cTime);


            Assert.AreEqual(frame, frameFromElement);
        }

        [TestMethod]
        public void TestFramePositionInterpolation()
        {
            Element element = new Element(0);
            ElementFrame status = new ElementFrame();

            element.SetFrame(ElementFrameHelper.CreateWithPosition(new Vector2(1, 1), 0.0f));
            element.SetFrame(ElementFrameHelper.CreateWithPosition(new Vector2(2, 1), 1.0f));
            element.SetFrame(ElementFrameHelper.CreateWithPosition(new Vector2(3, 2), 2.0f));

            status = UpdateElementStatus(element, 0.0f);
            ElementFrameHelper.CheckPositionInterpolation(status, new Vector2(1, 1));

            status = UpdateElementStatus(element, 0.5f);
            ElementFrameHelper.CheckPositionInterpolation(status, new Vector2(1.5f, 1));

            status = UpdateElementStatus(element, 1.0f);
            ElementFrameHelper.CheckPositionInterpolation(status, new Vector2(2, 1));

            status = UpdateElementStatus(element, 1.5f);
            ElementFrameHelper.CheckPositionInterpolation(status, new Vector2(2.5f, 1.5f));

            status = UpdateElementStatus(element, 2.0f);
            ElementFrameHelper.CheckPositionInterpolation(status, new Vector2(1.0f, 1f));
            
            status = UpdateElementStatus(element, 0.0f);
            ElementFrameHelper.CheckPositionInterpolation(status, new Vector2(1.0f, 1.0f));
        }

        [TestMethod]
        public void TestFramePositionInterpolationOneElement()
        {
            Element element = new Element(0);
            ElementFrame status = new ElementFrame();

            element.SetFrame(ElementFrameHelper.CreateWithPosition(new Vector2(5, 5), 1.0f));
            status = UpdateElementStatus(element, 0.5f);
            ElementFrameHelper.CheckPositionInterpolation(status, new Vector2(5.0f, 5.0f));
            status = UpdateElementStatus(element, 1.0f);
            ElementFrameHelper.CheckPositionInterpolation(status, new Vector2(5.0f, 5.0f));
            status = UpdateElementStatus(element, 1.5f);
            ElementFrameHelper.CheckPositionInterpolation(status, new Vector2(5.0f, 5.0f));
            status = UpdateElementStatus(element, 2.0f);
            ElementFrameHelper.CheckPositionInterpolation(status, new Vector2(5.0f, 5.0f));
        }

        private static ElementFrame UpdateElementStatus(Element element, float time)
        {
            ElementFrame status = element.GetInterpolation(time);
            element.SetCurrentStatus(status);
            return status;
        }

        [TestMethod]
        public void TestFrameRotationInterpolation()
        {
            Element element = new Element(0);

            element.SetFrame(ElementFrameHelper.CreateFrameElementWithRotation(new Vector2(0, 1), 0.0f));
            element.SetFrame(ElementFrameHelper.CreateFrameElementWithRotation(new Vector2(1, 0), 1.0f));
            element.SetFrame(ElementFrameHelper.CreateFrameElementWithRotation(new Vector2(0, -1), 2.0f));

            ElementFrameHelper.CheckRotationInterpolation(element.GetInterpolation(0.0f), 270);
            ElementFrameHelper.CheckRotationInterpolation(element.GetInterpolation(0.5f), 315);
            ElementFrameHelper.CheckRotationInterpolation(element.GetInterpolation(1.0f), 0);
            ElementFrameHelper.CheckRotationInterpolation(element.GetInterpolation(1.5f), 45);
            ElementFrameHelper.CheckRotationInterpolation(element.GetInterpolation(2.0f), 270);
        }


    }
}

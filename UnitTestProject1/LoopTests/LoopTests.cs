﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AniLib.Animations;

namespace UnitTestProject1.LoopTests
{
    [TestClass]
    public class LoopTests
    {
        [TestMethod]
        public void TestRepeatLoop()
        {
            RepeatLoop repeatLoop = new RepeatLoop();
            repeatLoop.SetTimes(0f, 1f);
            repeatLoop.SetLength(2f);

            Assert.AreEqual(1, repeatLoop.LoopTime(1f));
            Assert.AreEqual(1, repeatLoop.LoopTime(3f));
            Assert.AreEqual(1.5f, repeatLoop.LoopTime(3.5f));
        }

        [TestMethod]
        public void TestBounceLoop()
        {
            BounceLoop bounceLoop = new BounceLoop();
        }
    }
}

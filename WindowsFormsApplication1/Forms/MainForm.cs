﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using BrightIdeasSoftware;
using ObjectAnimator;
using AniLib.Animations;
using ObjectAnimator.Content;
using ObjectAnimator.Controls;
using AniLib.Math;

namespace WindowsFormsApplication1
{
    public partial class MainForm : Form
    {
        public class ElementRowData
        {
            public string Path;
            public Element Element;
        }
        public class FrameRowData
        {
            public float Time;
            public ElementFrame Frame;
        }

        private float mTime;
        private Element mCurrentElement;
        private float mSelectedFrameTime;

        private Timer mTimer;

        private Project mProject;

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            CreateProject();

            CreateLoopTimer();

            EnableDoubleBuffering();

            InitializeImageGetter();


            frameEditor1.DataChanged += FrameEditor1OnDataChanged;

            AnimationMgr.Instance.CreateNew();
        }

        private void FrameEditor1OnDataChanged(object sender, FrameEditor.DataChangedEventArgs e)
        {
            if (mSelectedFrameTime != -1)
            {
               ElementFrame frame = mCurrentElement.GetFrame(mSelectedFrameTime);
                frame.Position = e.NewData.Position;
                frame.Rotation = e.NewData.Rotation;
                mCurrentElement.SetFrame(frame);
            }
        }

        private void InitializeImageGetter()
        {
            OLVColumn column = elementsListView.GetColumn("Sprite");
            column.ImageGetter += ImageGetter;
        }

        private void CreateProject()
        {
            mProject = new Project();
            mProject.CreateNew(Environment.CurrentDirectory, "test");
        }


        private void CreateLoopTimer()
        {
            mTimer = new Timer(components);
            mTimer.Tick += TimerOnTick;
            mTimer.Interval = 16;
            mTimer.Start();
        }

        private void EnableDoubleBuffering()
        {
            splitContainer1.Panel1.GetType().InvokeMember("SetStyle",
                BindingFlags.NonPublic | 
                BindingFlags.InvokeMethod | 
                BindingFlags.Instance, null, splitContainer1.Panel1,
                new object[]
                {
                    ControlStyles.UserPaint |
                    ControlStyles.OptimizedDoubleBuffer |
                    ControlStyles.AllPaintingInWmPaint,
                    true
                });
        }

        private void TimerOnTick(object sender, EventArgs eventArgs)
        {
            mTime += 0.016f;
            timeLabel.Text = "Global time: " + mTime;
            AnimationMgr.Instance.PlayAnimationTo(mTime);

            splitContainer1.Refresh();
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AnimationMgr.Instance.CreateNew();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AnimationMgr.Instance.Load(mProject.GetPathToData());

            UpdateElementsListView();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AnimationMgr.Instance.Save(mProject.GetPathToData());
        }
        
        private void addElementButton_Click(object sender, EventArgs e)
        {
            FileDialog fileDialog = new OpenFileDialog();
            DialogResult result = fileDialog.ShowDialog();
            
            foreach (string fileName in fileDialog.FileNames)
            {
                AddElementFromFile(fileName);
            }

            UpdateElementsListView();
        }

        private void AddElementFromFile(string fileName)
        {
            mProject.CopyTextureFile(fileName);
            Element newElement = AnimationMgr.Instance.CurrentAnimation.AddNewElement();
            AnimationMgr.Instance.AddGraphicsElement(newElement.Id, fileName);
            UpdateFrameList(newElement);
        }

        private void UpdateElementsListView()
        {
            elementsListView.SetObjects(
                AnimationMgr.Instance.CurrentAnimation.Elements.Select(element =>
                    new ElementRowData
                    {
                        Path = AnimationMgr.Instance.GetGraphicsElement(element.Id),
                        Element = element
                    })
                );

            elementsListView.Refresh();
        }
        private void addFrameButton_Click(object sender, EventArgs e)
        {
            if (mCurrentElement == null)
            {
                return;
            }

            mCurrentElement.SetFrame(new ElementFrame
            {
                Time = mCurrentElement.AnimationLength + 0.1f,
                Alpha = 1.0f,
                Position = new Vector2(1f, 1f),
                Rotation = new Vector2(0f, 0f)
            });
            UpdateFrameList(mCurrentElement);

        }

        private void removeFrameButton_Click(object sender, EventArgs e)
        {
            if (mSelectedFrameTime != -1f)
            {
                mCurrentElement.RemoveFrame(mSelectedFrameTime);
                UpdateFrameList(mCurrentElement);
            }
        }

        private void removeElementButton_Click(object sender, EventArgs e)
        {
            if (mCurrentElement != null)
            {
                DialogResult dialogResult = MessageBox.Show("Are you sure you want to delete this element",
                                                            "Element deletion",
                                                            MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    AnimationMgr.Instance.CurrentAnimation.Remove(mCurrentElement);
                    UpdateElementsListView();
                }
            }
        }

        private void elementsListView_SelectionChanged(object sender, EventArgs e)
        {
            ElementRowData data = elementsListView.SelectedObject as ElementRowData;

            if (data == null)
            {
                return;
            }
            
            mCurrentElement = data.Element;

            mSelectedFrameTime = -1f;
            frameEditor1.SetElementFrame(mCurrentElement);
            UpdateFrameList(mCurrentElement);
        }

        private void frameListView_SelectionChanged(object sender, EventArgs e)
        {
            FrameRowData data = frameListView.SelectedObject as FrameRowData;

            if (data == null)
            {
                return;
            }

            mSelectedFrameTime = data.Time;
            frameEditor1.SetElementFrame(mCurrentElement.GetFrame(data.Time));
        }



        private void UpdateFrameList(Element element)
        {
            List<ElementFrame> frames = element.Frames;
            frameListView.SetObjects(frames.Select(p => new FrameRowData{ Time = p.Time, Frame = p}));
        }


        private object ImageGetter(object rowObject)
        {
            ElementRowData data = rowObject as ElementRowData;

            Image image = ContentMgr.Instance.GetImage(data.Path);

            ImageList.ImageCollection imageList = elementsListView.SmallImageList.Images;
            imageList.Add(image);

            int index = imageList.Count - 1;
            imageList.SetKeyName(index, data.Path);

            return data.Path;
        }

        private void splitContainer1_Panel1_Paint(object sender, PaintEventArgs e)
        {
            foreach (Element element in AnimationMgr.Instance.CurrentAnimation.Elements)
            {
                Image image = ContentMgr.Instance.GetImage(AnimationMgr.Instance.GetGraphicsElement(element.Id));
                e.Graphics.DrawImage(
                    image,
                    element.CurrentStatus.Position.X + element.Position.X,
                    element.CurrentStatus.Position.Y + element.Position.Y);
            }
        }

        private void frameListView_CellEditFinishing(object sender, CellEditEventArgs e)
        {
            dynamic row = e.RowObject;

            float newValue = (float) ((double) e.NewValue);
            newValue = Math.Max(0.005f, newValue);
            mCurrentElement.ChangeElementTime((float)((double)row.Time), newValue);
            row.Time = newValue;
        }

        private void frameListView_CellEditValidating(object sender, CellEditEventArgs e)
        {
        }


    }


    
}

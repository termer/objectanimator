﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectAnimator.Content
{
    public class ContentMgr : Singletone<ContentMgr>
    {
        private readonly Dictionary<string, Image> mCache = new Dictionary<string, Image>(); 

        public Image GetImage(string path)
        {
            if (mCache.ContainsKey(path))
            {
                return mCache[path];
            }

            Image image = new Bitmap(path);
            mCache[path] = image;
            return image;
        }
    }
}

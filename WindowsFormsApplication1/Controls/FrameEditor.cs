﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using AniLib.Animations;
using AniLib.Math;

namespace ObjectAnimator.Controls
{
    public partial class FrameEditor : UserControl
    {
        public class DataChangedEventArgs : EventArgs
        {
            public readonly IPositionable NewData;

            public DataChangedEventArgs(IPositionable data)
            {
                NewData = data;
            }
        }

        public delegate void DataChangedEventHandler(object sender, DataChangedEventArgs e);

        public event DataChangedEventHandler DataChanged;

        private IPositionable mElement;

        bool mInit; 

        public FrameEditor()
        {
            InitializeComponent();
        }

        public void SetElementFrame(IPositionable element)
        {
            mInit = true;
            mElement = element;

            UpdateFrameTexts();
        }

        private void TextBox_TextChanged(object sender, EventArgs e)
        {
            if (mInit)
            {
                return;
            }

            IPositionable newFrame = GetFrameFromTexts();
            if (DataChanged != null)
            {
                DataChanged(this, new DataChangedEventArgs(newFrame));
            }
        }

        private void UpdateFrameTexts()
        {
            positionXTextBox.Text = mElement.Position.X.ToString();
            positionYTextBox.Text = mElement.Position.Y.ToString();
            rotationTextBox.Text = FrameRotationDeg(mElement.Rotation).ToString();

            mInit = false;
        }

        private IPositionable GetFrameFromTexts()
        {
            Vector2 framePosition = mElement.Position;

            TryGetValue(ref framePosition.X, positionXTextBox);
            TryGetValue(ref framePosition.Y, positionYTextBox);
            mElement.Position = framePosition;

            float rotationAngle = FrameRotationDeg(mElement.Rotation);
            TryGetValue(ref rotationAngle, rotationTextBox);

            mElement.Rotation = Vector2.AngleToVector(rotationAngle * Mathf.DegToRad);

            return mElement;
        }

        private static void TryGetValue(ref float value, TextBox textBox)
        {
            float prevValue = value;
            if (!float.TryParse(textBox.Text, out value))
            {
                value = prevValue;
            }
        }


        private static float FrameRotationDeg(Vector2 direction) 
        {
            return (Vector2.VectorToAngle(direction) * Mathf.RadToDeg);
        }
    }
}

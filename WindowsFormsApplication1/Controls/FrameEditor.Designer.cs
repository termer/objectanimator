﻿namespace ObjectAnimator.Controls
{
    partial class FrameEditor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.positionLabel = new System.Windows.Forms.Label();
            this.positionXTextBox = new System.Windows.Forms.TextBox();
            this.positionYTextBox = new System.Windows.Forms.TextBox();
            this.rotationTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // positionLabel
            // 
            this.positionLabel.AutoSize = true;
            this.positionLabel.Location = new System.Drawing.Point(3, 19);
            this.positionLabel.Name = "positionLabel";
            this.positionLabel.Size = new System.Drawing.Size(44, 13);
            this.positionLabel.TabIndex = 0;
            this.positionLabel.Text = "Position";
            // 
            // positionXTextBox
            // 
            this.positionXTextBox.Location = new System.Drawing.Point(54, 16);
            this.positionXTextBox.Name = "positionXTextBox";
            this.positionXTextBox.Size = new System.Drawing.Size(101, 20);
            this.positionXTextBox.TabIndex = 1;
            this.positionXTextBox.TextChanged += new System.EventHandler(this.TextBox_TextChanged);
            // 
            // positionYTextBox
            // 
            this.positionYTextBox.Location = new System.Drawing.Point(161, 16);
            this.positionYTextBox.Name = "positionYTextBox";
            this.positionYTextBox.Size = new System.Drawing.Size(100, 20);
            this.positionYTextBox.TabIndex = 2;
            this.positionYTextBox.TextChanged += new System.EventHandler(this.TextBox_TextChanged);
            // 
            // rotationTextBox
            // 
            this.rotationTextBox.Location = new System.Drawing.Point(54, 42);
            this.rotationTextBox.Name = "rotationTextBox";
            this.rotationTextBox.Size = new System.Drawing.Size(101, 20);
            this.rotationTextBox.TabIndex = 4;
            this.rotationTextBox.TextChanged += new System.EventHandler(this.TextBox_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Rotation";
            // 
            // FrameEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.rotationTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.positionYTextBox);
            this.Controls.Add(this.positionXTextBox);
            this.Controls.Add(this.positionLabel);
            this.Name = "FrameEditor";
            this.Size = new System.Drawing.Size(291, 80);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label positionLabel;
        private System.Windows.Forms.TextBox positionXTextBox;
        private System.Windows.Forms.TextBox positionYTextBox;
        private System.Windows.Forms.TextBox rotationTextBox;
        private System.Windows.Forms.Label label1;
    }
}

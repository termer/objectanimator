﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ObjectAnimator.Content;

namespace ObjectAnimator
{
    public class Project
    {
        public struct ProjectData
        {
            public string Name;
            public string Path;
        }

        private ProjectData mProjectData;


        public void CreateNew(string path, string name)
        {
            mProjectData = new ProjectData
            {
                Name = name,
                Path = path
            };

            Save();
        }

        public void Save()
        {
            Utilites.SerializeObject<ProjectData>(mProjectData, GetFullPathToPrjFile());
        }

        public void Load()
        {
            
        }

        public void CopyTextureFile(string fileName)
        {
            FileInfo fileInfo = new FileInfo(fileName);
            string path = GetPathToTextures() + fileInfo.Name;

            Directory.CreateDirectory(GetPathToTextures());

            File.Copy(fileInfo.FullName, path, true);
        }

        public Image LoadTexture(string textureName)
        {
            string path = GetPathToTextures() + textureName;
            return ContentMgr.Instance.GetImage(path);
        }

        public string GetPath()
        {
            return mProjectData.Path;
        }

        public string GetPathToData()
        {
            return mProjectData.Path + "\\";
        }

        public string GetFullPathToPrjFile()
        {
            return mProjectData.Path + "\\" + mProjectData.Name + ".xml";
        }

        public string GetPathToTextures()
        {
            return Path.Combine(mProjectData.Path, "Textures") + "\\";
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectAnimator
{
    public abstract class Singletone<T> where T:class
    {
        private static T mInstance;

        public static T Instance
        {
            get
            {
                mInstance = mInstance ?? Activator.CreateInstance<T>();
                return mInstance;
            }
        }
    }
}

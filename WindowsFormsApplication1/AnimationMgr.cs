﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AniLib;
using AniLib.Animations;

namespace WindowsFormsApplication1
{
    public struct GraphicsElement
    {
        public int ElementId;
        public string GraphicsName;
    }

    public class AnimationMgr
    {
        private static AnimationMgr mInstance;
        
        private Animation mAnimation;


        protected AnimationMgr()
        {
            GraphicsElements = new HashSet<GraphicsElement>();
        }

        public void Save(string path)
        {
            Utilites.SerializeObject<Animation>(mAnimation, path + "data.xml");
            Utilites.SerializeObject<HashSet<GraphicsElement>>(GraphicsElements, path + "graphics.xml");
        }

        public void Load(string path)
        {
            mAnimation = Utilites.DeserializeObject<Animation>(path + "data.xml");
            GraphicsElements = Utilites.DeserializeObject<HashSet<GraphicsElement>>(path + "graphics.xml");
        }

        public void CreateNew()
        {
            mAnimation = new Animation();
        }

        public void PlayAnimationTo(float time)
        {
            mAnimation.InterpolateElementsTo(time);
        }

        public void AddGraphicsElement(int elementId, string graphicsName)
        {
            GraphicsElement element = new GraphicsElement
            {
                ElementId = elementId,
                GraphicsName = graphicsName
            };

            if (!GraphicsElements.Contains(element))
            {
                GraphicsElements.Add(element);
            }
        }

        public string GetGraphicsElement(int id)
        {
            return GraphicsElements.FirstOrDefault(p => p.ElementId == id).GraphicsName;
        }

        public HashSet<GraphicsElement> GraphicsElements; 

        public static AnimationMgr Instance
        {
            get
            {
                mInstance = mInstance ?? new AnimationMgr();    
                return mInstance;
            }
        }

        public Animation CurrentAnimation
        {
            get { return mAnimation; }
        }
    }
}
